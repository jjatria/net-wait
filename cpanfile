# We actually only depend on Net::EmptyPort
# See https://github.com/tokuhirom/Test-TCP/issues/63
requires 'Test::TCP';

on test => sub {
    requires 'Capture::Tiny';
    requires 'IO::Socket::INET';
    requires 'Test2::V0';
    requires 'Time::HiRes';
};
